//answer to incomint call
'use strict';
var tint = {timerInterval: null};

$(function () {
  let ringing = new Audio('sounds/ringing.mp3');
  let calling = new Audio('sounds/calling.mp3');
  let pressed_button = new Audio();
  let callstatus;
  ringing.loop = true;
  calling.loop = true;
    
  $(document).on('incomingcall', function (ev, from) {
													    //    noRingtone();
													    //    ringing.play();
													    //    $('#hangup-btn,#answer-btn').removeAttr('disabled');
													    //    $('#call-btn,#call-to').attr('disabled','disabled');
													    //    $('#output-lbl').text('...incoming call from ' + from);
    $("body").addClass("voice_call__inc");                                                          // add incoming call class
    $(".b-nav").addClass("close");                                                                          // hide black buttons and do 'answer call' buttons visible
    $(".b-nav__inc, .call__inc__dial").addClass("open");
    $(".call__inc__dial"). addClass("open").text(from);
    ringing.play();
    console.log('calling from : ' + from );
    //console.log("session: " + webrtcPhone.getRTCSession());
    //console.log(ev);
    let sess = webrtcPhone.getRTCSession();
    //let phones = webrtcPhone.getPhone();

    let checksess = setInterval(function() {
        console.log(sess.status);
        //console.log(phones);
        if (sess.status === 9) {
            $(document).trigger('hangup');
            clearInterval(checksess);         
        };
        if (sess.status === 12) {
            clearInterval(checksess);         
        };
    },2000);
});

$(".b-nav__answer").on("click", function () { 
    $(".b-nav__cancel_call span").text("Hangup");
    if ($("body").hasClass("voice_call__inc")) {        // if the call is incoming
													    //        	$(".voice_call__call").addClass("open");        // open talk view
													    //                if (this.incomingCall) {
													    //                    $(".interlocutor2").text(call.caller());
													    //                } else {
													    //                    $(".interlocutor2").text(call.callee());
													    //                }
	}
													        //alert("AAudio");
    $(".hook").addClass("open");
    $(".interlocutor2").text($(".call__inc__dial").text());
    $(".interlocutor1").text(webrtcPhone.getExten());   
													    //    $(".b-video").addClass("open");
    webrtcPhone.answer();
});


$(".b-display__header__login").on("click", function () { 
    webrtcPhone.initAndLogin({
      domain: 'destination.domain',
      server: 'webrtc2sip.yooxy.ru',
      name: 'name',
      exten: 'exten',
      password: 'password',
      audioId: 'remoteAudio',
//      localVideoId: 'localVideo',
//      remoteVideoId: 'remoteVideo'
    });

													    //    alert("Pressed");

});

$(document).on('hangup', function (ev) {
        $(".call__out__dial").text("calling to");                                       	// return to initial view of outgoing call (view without number or login name)
        $("body").removeAttr("class");                                                          // remove all classes from the body
        $(".b-mike, .call__out__dial, .call__inc__dial, .voice_call__call, .voice_call__play, .voice_call__call__pause, .b-transfer, .b-video, .b-video__video, .b-nav__inc, .b-alert").removeClass("open"); // close set of blocks which are hidden by default
        $(".b-display__bottom__number>span, .voice_call__call__play, .voice_call__transfer, .b-nav").removeClass("close");      // open a set of blocks which might be hidden, but the blocks are visible by default
        $(".b-time").html("<span class='b-min'>00</span>:<span class='b-sec'>00</span>");	
        $(".b-numbers").removeClass("write");
        $(".b-numbers__clear").removeClass("open");
        $(".b-display__bottom__number>span").removeClass("close");
        $(".b-numbers").val("");
        clearInterval(tint.timerInterval);
        tint.timerInterval = null;
        ringing.pause();
        callstatus = "ENDED";
        console.log(callstatus);
  });
// do video and audio call

    $(".b-nav__voice, .b-nav__video").on("click", function () {
        
        if ($(".b-numbers").hasClass("write") 
            && $(".b-numbers").val().length > 1
            && webrtcPhone.getPhone().isRegistered()) {  // check if phone number was entered and digits more then 1 length
            $("body").addClass("voice_call__out");
            $(".call__out__dial").addClass("open").html($(".call__out__dial").html() + " " + $(".b-numbers").val());    // open outgoing call block and writ here callee number
//            if ($(this).hasClass("b-nav__video")) {
//                $("body").addClass("video")
//            } // if it is a video call then add the body class (for alert view)

//            if ($("body").hasClass("video")) {
//                webrtcPhone.call($(".b-numbers").val(), true);
//            } else {
                webrtcPhone.call($(".b-numbers").val(), false);
//            }

        }
    });



//Call answered by endpoint:

  $(document).on('callaccepted', function (ev) {
        ringing.pause();
        callstatus = "ACCEPTED";
        console.log(callstatus);
	    $(".b-alert, .b-nav__inc, .call__inc__dial").removeClass("open"); 	// hide a set of buttons and buttons allow/deny
            $(".b-nav").removeClass("close");   				// re-open default navigation buttons (i.e. if the buttons was hidden while an incoming call)
//            if ($("body").hasClass("video")) {
//                $(".b-video__video").addClass("open");
//                $(".b-video, .hook").addClass("open");
													    	        //alert("Video");
//            } else {
													    	        //alert("AAudio");
               $(".hook").addClass("open");
//            }

													    //            $(".b-nav__cancel_call span").text("Hangup");       // change text of "Cancel" button
            if ($("body").hasClass("voice_call__inc")) {        // if the call is incoming
                $(".voice_call__call").addClass("open");        // open talk view
													    //                if (this.incomingCall) {
													    //                    $(".interlocutor2").text(call.caller());
													    //                } else {
													    //                    $(".interlocutor2").text(call.callee());
													    //                }
            } else {
                $(".call__out__dial").removeClass("open");      // hide call view
                $(".voice_call__call").addClass("open");        // open talk view
                $(".b-nav__cancel_call span").text("Hangup");   // change text for Hangup button
                $(".interlocutor2").text($(".b-numbers").val());
                $(".interlocutor1").text(webrtcPhone.getExten());        // set number of callee in the talk view
               // $(this).removeClass("open");   
              //  console.log(Object.values(this));                  // hide 'Answer' button
            }

            
            StartTimer(tint);
                   

            $(".voice_call__transfer").removeClass("close");
            $(".voice_call__stop").addClass("open");            // do visible hold button
            $(".voice_call__play").removeClass("open");                                 // hide unhold button
            $(".voice_call__call__pause").removeClass("open");  // hide hold view and transfer view (if the views was opened)
            $(".voice_call__call__play").removeClass("close");                          // open talk view
            $(".voice_call__transfer").removeClass("tr_call__pause");           // remove class from the transfer button (if the class exists, then after transfer cancellation)

});

function StartTimer(tint) {
    var me = tint;

    if (!me.timerInterval) {
        me.start = 0;
        me.min = 0;
        me.timerInterval = setInterval(function () {
            me.start++;
            if (me.start > 59) {
                me.start = 0;
                me.min++;
                if (me.min < 10) {
                    $(".b-min").html("0" + me.min);
                } else $(".b-min").html(me.min);
            }
            if (me.start < 10) {
                $(".b-sec").html("0" + me.start);
            } else $(".b-sec").html(me.start);
        }, 1000);
    }
}


  $(document).on('registered', function (ev) {
     $(".b-display__header__login").text("Reg. ok");
     $(".b-numbers__clear").addClass("open");
     $(".b-numbers").addClass("write").focus();
     

  });
     
  // removing of symbols
     $(".b-numbers__clear").on("click", function () {
        $(".b-numbers").val($(".b-numbers").val().substring(0, $(".b-numbers").val().length - 1));
        var num = $(".b-numbers").val().length;
        if (num == 0) {	// if the latest digit was removed then return to initial position
            //$(this).removeClass("open");
            $(".b-numbers").removeClass("write");
            $(".b-display__bottom__number>span").removeClass("close");
        }
    });
// enter phone number
$(".b-display__bottom__number>span").on("click", function () { // on click by 'Enter your number here'
    //if (phone.connectionStatus == SESSION_STATUS.ESTABLISHED || phone.connectionStatus == SESSION_STATUS.REGISTERED) {
    //    $(this).addClass("close");
        $(".b-numbers__clear").addClass("open");
        $(".b-numbers").addClass("write").focus();
    //}
});

  $(document).keyup(function (event) {			
      					// listening keyboard input
    if (($(".b-numbers").is(":focus")) && (event.keyCode == 8)) {	// if the focus on the number field and Backspace is pressed
        console.log($(".b-numbers").val().length);
        var num = $(".b-numbers").val().length;						// check number of symbols in the field which remain after the symbol removing
        if (num == 0) {											// if the latest digit was removed then return to initial position
            $(".b-numbers__clear").removeClass("open");
            $(".b-numbers").removeClass("write");
            $(".b-display__bottom__number>span").removeClass("close");
        }
    }
    });

  $(".b-num td").on("click", function () {
//        if (phone.connectionStatus == SESSION_STATUS.ESTABLISHED || phone.connectionStatus == SESSION_STATUS.REGISTERED) {

    console.log("Pressed buttons. play beep" + $(this).text());
    pressed_button.src = "sounds/" + $(this).text() +".ogg";
    pressed_button.play();

    //if (webrtcPhone.getRTCSession()) {
        if (callstatus == "ACCEPTED") {
        console.log(callstatus);
        console.log("Session is active" + webrtcPhone.getRTCSession());
        webrtcPhone.getRTCSession().dtmf($(this).text());
        }    
        if (!$(".b-numbers").hasClass("write")) {   // if the symbols are entered then remove the text block
            $(".b-numbers").val("");
            $(".b-numbers").addClass("write").next().addClass("open"); 
            $(".b-display__bottom__number>span").addClass("close");
             
            }
//            if (phone.currentCall &&
//                (CALL_STATUS.ESTABLISHED == phone.currentCall.status() || CALL_STATUS.HOLD == phone.currentCall.status())) {
//                phone.sendDTMF($(this).text());
//            } else {
//                if ($(".b-transfer").hasClass("open")) {
//                    $("#transfer").val($("#transfer").val() + $(this).text());
//                } else if (!phone.currentCall) {
                    $(".b-numbers").val($(".b-numbers").val() + $(this).text());
//                }
//            }
//        }
    });
    // do video and audio call

    $(".b-nav__cancel_call, .close, .b-nav__hangup").on("click", function () {  		// return to initial view
        console.log("hangup by click");
        ringing.pause();
        webrtcPhone.hangup();
	    $(".call__out__dial").text("calling to");                                       	// return to initial view of outgoing call (view without number or login name)
        $("body").removeAttr("class");                                                          // remove all classes from the body
        $(".b-mike, .call__out__dial, .call__inc__dial, .voice_call__call, .voice_call__play, .voice_call__call__pause, .b-transfer, .b-video, .b-video__video, .b-nav__inc, .b-alert").removeClass("open"); // close set of blocks which are hidden by default
        $(".b-display__bottom__number>span, .voice_call__call__play, .voice_call__transfer, .b-nav").removeClass("close");      // open a set of blocks which might be hidden, but the blocks are visible by default
//        $(".b-alert").text("").removeClass("video_alert");      				// initial view of video alert

    });
    
    $(".voice_call__transfer").on("click", function () {							// if the transfer button is pressed
        //if (phone.currentCall) {
          //  phone.holdForTransfer(phone.currentCall);
            $(".voice_call__transfer").addClass("close");		// hide transfer button
            $(".b-transfer").addClass("open");				// open transfer view and hold view
            $(".b-transfer").attr("id", "active");
            $("#transfer").focus();														// set focus on the phone number field
        //}
    });

    $(".b-transfer__ok").on("click", function () {
        console.log("Enter to transfer");
        if (webrtcPhone.getRTCSession()) {
            webrtcPhone.getRTCSession().refer($("#transfer").val());
        }
        $(".voice_call__transfer").removeClass("close");
        $(".b-transfer").removeClass("open").removeAttr("id");

    });

    $(".voice_call__stop").on("click", function () {	// if the call is on hold
        if (webrtcPhone.getRTCSession()) {
            webrtcPhone.getRTCSession().hold();
            $(".voice_call__transfer").addClass("close");
            $(this).removeClass("open");					// hide the hold button
            $(".voice_call__play").addClass("open");	// do visible button of returning to the call and hold view
        }
    });

    $(".voice_call__play").on("click", function () {	// return to talk
        if (webrtcPhone.getRTCSession()) {
            webrtcPhone.getRTCSession().unhold();
            $(".voice_call__transfer").removeClass("close");
            $(this).removeClass("open");					// hide unhold button
            $(".voice_call__stop").addClass("open");						// do visible hold button
        }
    });

});